import { Component, ViewEncapsulation } from '@angular/core';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';

@Component({
  selector: 'app-info-panels',
  templateUrl: './info-panels.component.html',
  encapsulation: ViewEncapsulation.None
})
export class InfoPanelsComponent {
  
  public sales = [{ name:'Verified', value:581 }];
  public salesBgColor = { domain: ['#2F3E9E'] };

  public likes = [{ name:'Pre-registered', value:4021 }];
  public likesBgColor = { domain: ['#D22E2E'] };

  public downloads = [{ name:'Checked In', value:800 }];
  public downloadsBgColor = { domain: ['#378D3B'] };

  public profit = [{ name:'Blocked', value:120 }];
  public profitBgColor = { domain: ['#0096A6'] };

  public messages = [{ name:'Checked Out', value:984 }];
  public messagesBgColor = { domain: ['#606060'] };

  public members = [{ name:'Watchlist', value:684 }];
  public membersBgColor = { domain: ['#F47B00'] };


  public infoLabelFormat(c): string {
    switch(c.data.name) {
      case 'Verified':
        return `<i class="fa fa-thumbs-up mr-2"></i>${c.label}`;
      case 'Pre-registered':
        return `<i class="fa fa-calendar-check-o mr-2"></i>${c.label}`;
      case 'Checked In':
        return `<i class="fa fa-map-marker mr-2"></i>${c.label}`;
      case 'Blocked':
        return `<i class="fa fa-ban mr-2"></i>${c.label}`;
      case 'Checked Out':
        return `<i class="fa fa-sign-out  mr-2"></i>${c.label}`;
      case 'Watchlist':
        return `<i class="fa fa-eye mr-2"></i>${c.label}`;
      default:
        return c.label;
    }
  }

  public infoValueFormat(c): string {
    switch(c.data.extra ? c.data.extra.format : '') {
      case 'currency':
        return `\$${Math.round(c.value).toLocaleString()}`;
      case 'percent':
        return `${Math.round(c.value * 100)}%`;
      default:
        return c.value.toLocaleString();
    }
  }

  public previousShowMenuOption:boolean;
  public previousMenuOption:string;
  public previousMenuTypeOption:string;
  public settings: Settings;
  constructor(public appSettings:AppSettings) {    
    this.settings = this.appSettings.settings;
    this.initPreviousSettings();
  }

  public onSelect(event) {
    console.log(event);
  }



  public ngDoCheck() {
    if(this.checkAppSettingsChanges()) {
      setTimeout(() => this.sales = [...this.sales] ); 
      setTimeout(() => this.likes = [...this.likes] ); 
      setTimeout(() => this.downloads = [...this.downloads] ); 
      setTimeout(() => this.profit = [...this.profit] ); 
      setTimeout(() => this.messages = [...this.messages] ); 
      setTimeout(() => this.members = [...this.members] ); 
      this.initPreviousSettings();
    }
  }

  public checkAppSettingsChanges(){
    if(this.previousShowMenuOption != this.settings.theme.showMenu ||
      this.previousMenuOption != this.settings.theme.menu ||
      this.previousMenuTypeOption != this.settings.theme.menuType){
      return true;
    }
    return false;
  }

  public initPreviousSettings(){
    this.previousShowMenuOption = this.settings.theme.showMenu;
    this.previousMenuOption = this.settings.theme.menu;
    this.previousMenuTypeOption = this.settings.theme.menuType;
  }

}
