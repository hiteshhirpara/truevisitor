import { Component, OnInit } from '@angular/core';
/* import { Component, ViewEncapsulation } from '@angular/core'; */
declare  var $:  any;

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss'],
/*   encapsulation: ViewEncapsulation.None */
})
export class ClientComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
		$(document).ready(function(){
			$(function () {
			  $('[data-toggle="tooltip"]').tooltip()
			})
			$('.form-check').click(function(){
				$(this).tooltip('hide')
			});
			$('.btn-1').click(function(){
				$('.one').fadeOut();   	
				$('.two').fadeIn();   	
			});
			$('.btn-2').click(function(){
				$('.step-verify-phone').fadeOut();   	
				$('.step-all').fadeIn();   	
			});
			$('.btn-3').click(function(){
				$('.step-personal-detail').fadeOut();   	
				$('.step-business-detail').fadeIn();
				$('.pd-tab').removeClass("bg-danger").addClass("bg-success");				
				$('.bd-tab').addClass("bg-danger");				
			});
			$('.btn-4').click(function(){
				$('.step-business-detail').fadeOut();   	
				$('.step-location').fadeIn();
				$('.bd-tab').removeClass("bg-danger").addClass("bg-success");				
				$('.loc-tab').addClass("bg-danger");				
			});
			$('.btn-5').click(function(){
				$('.step-location').fadeOut();
				$('.step-department').fadeIn();
				$('.loc-tab').removeClass("bg-danger").addClass("bg-success");
				$('.dep-tab').addClass("bg-danger");
			});
			$('.btn-6').click(function(){
				$('.step-all').fadeOut();
				$('.thankyou-content').fadeIn();
			});
			
		});
		document.getElementById('preloader').classList.add('hide');
  }

}
